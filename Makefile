all:
	g++ polynomial.cpp tokens.cpp parsedprogram.cpp lexer.cpp main.cpp -o program.o

polynomial: polynomial.cpp polynomial.test.cpp
	g++ polynomial.cpp polynomial.test.cpp -Wall -Werror -o polynomial.o
