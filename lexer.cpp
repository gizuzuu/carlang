#include "lexer.hpp"
#include "polynomial.hpp"
#include "tokens.hpp"
#include <cstdio>

using namespace lex;

Lexer::Lexer()
{
  this->lineNumber = 1;
  this->nameIndex = -1;

  for (int i = 0; i < STATES_COUNT; ++i)
  {
    for (int j = 0; i < tkns::SYMBOL_TOKEN_CLASS_COUNT; ++j)
    {
      this->table[i][j] = &Lexer::Err1;
    }
  }

  // Таблица обнаружения
  for (int i = 0; i < 26; ++i)
  {
    this->detectionTable.initVector[i] = -1;
  }

  // Начальный вектор
  detectionTable.initVector['b' - 'a'] = 0;  // begin
  detectionTable.initVector['c' - 'a'] = 4;  // cout, cin
  detectionTable.initVector['d' - 'a'] = 9;  // declare
  detectionTable.initVector['e' - 'a'] = 16; // else, end
  detectionTable.initVector['j' - 'a'] = 22; // jump
  detectionTable.initVector['t' - 'a'] = 25; // to, then
  detectionTable.initVector['s' - 'a'] = 29; // set
  detectionTable.initVector['p' - 'a'] = 31; // polynomial
  detectionTable.initVector['i' - 'a'] = 40; // int, if

  for (int i = 0; i < DETECTION_TABLE_SIZE; ++i)
  {
    detectionTable.table[i].alt = -1;
    detectionTable.table[i].procedure = &Lexer::B1b;
  }

  // Begin
  detectionTable.table[0].letter = 'e';
  detectionTable.table[1].letter = 'g';
  detectionTable.table[2].letter = 'i';
  detectionTable.table[3].letter = 'n';
  detectionTable.table[3].procedure = &Lexer::B1b; // TODO: Add begin token

  // Cin
  detectionTable.table[4].letter = 'i';
  detectionTable.table[4].alt = 6;
  detectionTable.table[5].letter = 'n';
  detectionTable.table[5].procedure = &Lexer::B1b; // TODO: Add cin token

  // Cout
  detectionTable.table[6].letter = 'o';
  detectionTable.table[7].letter = 'u';
  detectionTable.table[8].letter = 't';
  detectionTable.table[8].procedure = &Lexer::B1b; // TODO: Add cout token

  // Declare
  detectionTable.table[9].letter = 'e';
  // detectionTable.table[9].alt = 15;
  detectionTable.table[10].letter = 'c';
  detectionTable.table[11].letter = 'l';
  detectionTable.table[12].letter = 'a';
  detectionTable.table[13].letter = 'r';
  detectionTable.table[14].letter = 'e';
  detectionTable.table[14].procedure = &Lexer::B1b; // TODO: Add declare token

  // Do (Unused)
  detectionTable.table[15].letter = 'o';
  detectionTable.table[15].procedure = &Lexer::B1b; // TODO: Add do token

  // End
  detectionTable.table[16].letter = 'n';
  detectionTable.table[16].alt = 19;
  detectionTable.table[17].letter = 'd';
  detectionTable.table[18].procedure = &Lexer::B1b; // TODO: Add end token

  // Else
  detectionTable.table[19].letter = 'l';
  detectionTable.table[20].letter = 's';
  detectionTable.table[21].letter = 'e';
  detectionTable.table[21].procedure = &Lexer::B1b; // TODO: Add else token

  // Jump
  detectionTable.table[22].letter = 'u';
  detectionTable.table[23].letter = 'm';
  detectionTable.table[24].letter = 'p';
  detectionTable.table[24].procedure = &Lexer::B1b; // TODO: Add jump token

  // To
  detectionTable.table[25].letter = 'o';
  detectionTable.table[25].procedure = &Lexer::B1b; // TODO: Add to token

  // Then
  detectionTable.table[26].letter = 'h';
  detectionTable.table[27].letter = 'e';
  detectionTable.table[28].letter = 'n';
  detectionTable.table[28].procedure = &Lexer::B1b; // TODO: Add then token

  // Set
  detectionTable.table[29].letter = 'e';
  detectionTable.table[30].letter = 't';
  detectionTable.table[30].procedure = &Lexer::B1b; // TODO: Add set token

  // Polynomial
  detectionTable.table[31].letter = 'o';
  detectionTable.table[32].letter = 'l';
  detectionTable.table[33].letter = 'y';
  detectionTable.table[34].letter = 'n';
  detectionTable.table[35].letter = 'o';
  detectionTable.table[36].letter = 'm';
  detectionTable.table[37].letter = 'i';
  detectionTable.table[38].letter = 'a';
  detectionTable.table[39].letter = 'l';
  detectionTable.table[39].procedure = &Lexer::B1b; // TODO: Add polynomial token

  // Int
  detectionTable.table[40].letter = 'n';
  detectionTable.table[40].alt = 42;
  detectionTable.table[41].letter = 't';
  detectionTable.table[41].procedure = &Lexer::B1b; // TODO: Add int token

  // If
  detectionTable.table[42].letter = 'f';
  detectionTable.table[42].procedure = &Lexer::B1b; // TODO: Add if token
}

// Lexer transliterator
tkns::SymbolicToken Lexer::Transliterate(int symbol)
{
  tkns::SymbolicToken token;
  token.value = 0;

  if (('a' <= symbol && symbol <= 'z') || ('A' <= symbol && symbol <= 'Z'))
  {
    token.clazz = tkns::SymbolicTokenClass::Letter_ST;
    token.value = symbol;
  }
  else if ('0' <= symbol && symbol <= '9')
  {
    token.clazz = tkns::SymbolicTokenClass::Digit_ST;
    token.value = symbol - '0';
  }
  else if (symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/' || symbol == '%')
  {
    token.clazz = tkns::SymbolicTokenClass::Arithmetic_ST;
    token.value = symbol;
  }
  else if (symbol == '<' || symbol == '>' || symbol == '=' || symbol == '!')
  {
    token.clazz = tkns::SymbolicTokenClass::Compare_ST;

    switch (symbol)
    {
    case '<':
      token.value = tkns::CompareValue::Less;
      break;
    case '>':
      token.value = tkns::CompareValue::Bigger;
      break;
    case '=':
      token.value = tkns::CompareValue::Equal;
      break;
    case '!':
      token.value = tkns::CompareValue::Negation;
      break;
    }
  }
  else if (symbol == ' ' || symbol == '\t')
  {
    token.clazz = tkns::SymbolicTokenClass::Space_ST;
  }
  else if (symbol == '\n')
  {
    token.clazz = tkns::SymbolicTokenClass::LF_ST;
  }
  else if (symbol == ';')
  {
    token.clazz = tkns::SymbolicTokenClass::Semicolon_ST;
  }
  else if (symbol == ',')
  {
    token.clazz = tkns::SymbolicTokenClass::Comma_ST;
  }
  else if (symbol == ':')
  {
    token.clazz = tkns::SymbolicTokenClass::Colon_ST;
  }
  else if (symbol == '[')
  {
    token.clazz = tkns::SymbolicTokenClass::LSqBracket_ST;
  }
  else if (symbol == ']')
  {
    token.clazz = tkns::SymbolicTokenClass::RSqBracket_ST;
  }
  else if (symbol == '(')
  {
    token.clazz = tkns::SymbolicTokenClass::LBracket_ST;
  }
  else if (symbol == ')')
  {
    token.clazz = tkns::SymbolicTokenClass::RBracket_ST;
  }
  else if (symbol == EOF)
  {
    token.clazz = tkns::SymbolicTokenClass::Eof_ST;
  }
  else
  {
    token.clazz = tkns::SymbolicTokenClass::Other_ST;
  }

  return token;
}

// Lexer adding
void Lexer::AddToken()
{
  this->tokens.push_back(tkns::Token(this->tokenClass, this->tokenValue, this->lineNumber));
}

void Lexer::AddConstant()
{
  for (int i = 0; i < this->nameTable.size(); ++i)
  {
    if (this->nameTable[i].GetType() != tkns::ObjectType::Constant)
      continue;

    if (*((int *)this->nameTable[i].GetPointer()) == this->number)
    {
      this->nameIndex = i;
      return;
    }
  }

  int *newConstant = new int(this->number);
  nameTable.push_back(tkns::ObjectName(tkns::ObjectType::Constant, newConstant));
  this->nameIndex = this->nameTable.size() - 1;
}

void Lexer::AddPolynomialConstant()
{
  for (int i = 0; i < this->nameTable.size(); ++i)
  {
    if (this->nameTable[i].GetType() != tkns::ObjectType::Polynomial)
      continue;

    if (*((pol::Polynomial *)this->nameTable[i].GetPointer()) == this->polynomial)
    {
      this->nameIndex = i;
      return;
    }
  }

  pol::Polynomial *new_polynomial = new pol::Polynomial(this->polynomial);
  this->nameTable.push_back(tkns::ObjectName(tkns::ObjectType::Polynomial, new_polynomial));
  this->nameIndex = this->nameTable.size() - 1;
}

void Lexer::AddVariable()
{
  for (int i = 0; i < this->nameTable.size(); ++i)
  {
    if (this->nameTable[i].GetType() != tkns::ObjectType::Variable)
      continue;

    if (*((std::string *)this->nameTable[i].GetPointer()) == this->variableName)
    {
      this->nameIndex = i;
      return;
    }
  }

  std::string *new_variable = new std::string(this->variableName);
  this->nameTable.push_back(tkns::ObjectName(tkns::ObjectType::Polynomial, new_variable));
  this->nameIndex = this->nameTable.size() - 1;
}

void Lexer::AddLabel()
{
  for (int i = 0; i < this->nameTable.size(); ++i)
  {
    if (this->nameTable[i].GetType() != tkns::ObjectType::Label)
      continue;

    if (*((std::string *)this->nameTable[i].GetPointer()) == this->label)
    {
      this->nameIndex = i;
      return;
    }
  }

  std::string *new_label = new std::string(this->label);
  this->nameTable.push_back(tkns::ObjectName(tkns::ObjectType::Label, new_label));
  this->nameIndex = this->nameTable.size() - 1;
}

// Lexer automats
LexerState Lexer::A1()
{
  return LexerState::A1;
}

LexerState Lexer::A1a()
{
  this->lineNumber++;
  return LexerState::A1;
}

LexerState Lexer::A1b()
{
  this->tokenClass = tkns::TokenClass::Error;
  this->tokenValue = 0;
  this->AddToken();
  this->lineNumber++;
  return LexerState::A1;
}

LexerState Lexer::A1c()
{
  this->AddVariable();
  this->tokenValue = this->nameIndex;
  this->tokenClass = tkns::TokenClass::Variable;
  this->AddToken();
  this->lineNumber++;
  return LexerState::A1;
}

LexerState Lexer::A1d()
{
  this->AddConstant();
  this->tokenValue = this->nameIndex;
  this->tokenClass = tkns::TokenClass::Constant;
  this->AddToken();
  this->lineNumber++;
  return LexerState::A1;
}

LexerState Lexer::A1f()
{
  this->AddLabel();
  this->tokenValue = this->nameIndex;
  this->tokenClass = tkns::TokenClass::Label;
  this->AddToken();
  this->lineNumber++;
  return LexerState::A1;
}

LexerState Lexer::A1g()
{
  this->AddToken();
  this->lineNumber++;
  return LexerState::A1;
}

State A1h()
	{
		switch (tokenClass)
		{
		case Jump_T: Err2(); break;
		default:
		{
			AddToken();
			lineNumber++;
			return State::st_A1;
		}
		}
	}

LexerState Lexer::A1l()
{
  this->tokenClass = tkns::TokenClass::Comment;
  this->tokenValue = 0;
  this->AddToken();
  this->lineNumber++;
  return LexerState::A1;
}

LexerState Lexer::B1()
{
  return LexerState::B1;
}

LexerState Lexer::B1a()
{
  this->variableName = this->symbolicToken.value;

  if ('A' <= this->symbolicToken.value && this->symbolicToken.value <= 'Z')
    return B2();

  detectionIndex = detectionTable.initVector[this->symbolicToken.value - 'a'];
  if (detectionIndex == -1)
    return B2();
  return LexerState::B1;
}

LexerState Lexer::B1b()
{
  this->detectionIndex++;
  return LexerState::B1;
}

LexerState Lexer::B2()
{
  return LexerState::B2;
}

LexerState Lexer::B2a()
{
  if (this->symbolicToken.value < 10)
    this->variableName += (this->symbolicToken.value + '0');
  else
    this->variableName += this->symbolicToken.value;
  return LexerState::B2;
}

LexerState Lexer::B3()
{
  return LexerState::B3;
}

LexerState Lexer::C1()
{
  return LexerState::C1;
}

LexerState Lexer::C1a()
{
  this->number = this->symbolicToken.value;
  return LexerState::C1;
}

LexerState Lexer::C1b()
{
  this->number = 10 * this->number + this->symbolicToken.value;
  return LexerState::C1;
}

LexerState D1()
{
  return LexerState::D1;
}

LexerState Lexer::D1a()
{
  this->tokenValue = this->compareValue = (tkns::CompareValue)this->symbolicToken.value;

  this->tokenClass = tkns::TokenClass::CompareOperator;

  if (this->compareValue == tkns::CompareValue::Equal)
    this->tokenClass = tkns::TokenClass::Assigment;

  return LexerState::D1;
}

LexerState Lexer::D2()
{
  return LexerState::D2;
}

LexerState Lexer::D2a()
{
  if (this->symbolicToken.value == tkns::CompareValue::Equal)
  {
    switch (this->compareValue)
    {
    case tkns::CompareValue::Negation:
      this->tokenValue = tkns::CompareValue::NotEqual;
      break;
    case tkns::CompareValue::Equal:
      this->tokenValue = tkns::CompareValue::Equal;
      break;
    default:
      this->tokenValue = this->compareValue + this->symbolicToken.value;
      break;
    }
  }
  else if (this->compareValue == tkns::CompareValue::Less && this->symbolicToken.value == tkns::CompareValue::Less)
  {
    this->tokenClass = tkns::TokenClass::StreamOut;
    this->tokenValue = 0;
    return LexerState::I1; // Вывод
  }
  else if (this->compareValue == tkns::CompareValue::Bigger && this->symbolicToken.value == tkns::CompareValue::Bigger)
  {
    this->tokenClass = tkns::TokenClass::StreamIn;
    this->tokenValue = 0;
    return LexerState::I1; // Ввод
  }
  else
    return Err1();

  this->tokenClass = tkns::TokenClass::CompareOperator;
  return LexerState::D2;
}

LexerState Lexer::H1()
{
  return LexerState::H1;
}

LexerState Lexer::H2()
{
  return LexerState::H2;
}

LexerState Lexer::H2a()
{
  if (this->symbolicToken.value == tkns::CompareValue::Bigger)
    return LexerState::H2;

  return Err1();
}

LexerState Lexer::H3()
{
  return LexerState::H3;
}

LexerState Lexer::H3a()
{
  if (this->symbolicToken.value == tkns::CompareValue::Bigger)
    return LexerState::H3;

  return Err1();
}

LexerState Lexer::H4()
{
  return LexerState::H3;
}

LexerState Lexer::H4a()
{
  if (this->symbolicToken.value == tkns::CompareValue::Bigger)
    return LexerState::H4;

  return this->Err1();
}

LexerState Lexer::G1()
{
  return LexerState::G1;
}

LexerState Lexer::G1a()
{
  this->label = this->symbolicToken.value;
  return LexerState::G1;
}

LexerState Lexer::G2()
{
  return LexerState::G2;
}

LexerState Lexer::G2a()
{
  if (this->symbolicToken.value < 10)
    this->label += (this->symbolicToken.value + '0');
  else
    this->label += this->symbolicToken.value;

  return LexerState::G2;
}

LexerState Lexer::G3()
{
  return LexerState::G3;
}

LexerState Lexer::G3a()
{
  if (this->symbolicToken.value == tkns::CompareValue::Bigger)
    return LexerState::G3;

  return this->Err1();
}

LexerState Lexer::G4()
{
  return LexerState::G4;
}

LexerState Lexer::G4a()
{
  if (this->symbolicToken.value == tkns::CompareValue::Bigger)
    return LexerState::G4;

  return this->Err1();
}

LexerState Lexer::E1()
{
  return LexerState::E1;
}

LexerState Lexer::E1a()
{
  this->tokenClass = tkns::TokenClass::ArithmeticOperator;
  this->tokenValue = this->symbolicToken.value;
  return LexerState::E1;
}

LexerState Lexer::L1()
{
  return LexerState::L1;
}

LexerState Lexer::L1a()
{
  this->label = this->symbolicToken.value;
  return LexerState::L2;
}

LexerState Lexer::L2()
{
  return LexerState::L2;
}

LexerState Lexer::L2a()
{
  if (this->symbolicToken.value < 10)
    this->label += (this->symbolicToken.value + '0');
  else
    this->label += this->symbolicToken.value;

  return LexerState::L2;
}

LexerState Lexer::M1()
{
  this->variableName += this->symbolicToken.value;

  while (true)
  {
    if (this->detectionTable.table[this->detectionIndex].letter == this->symbolicToken.value)
      return (this->*detectionTable.table[this->detectionIndex].procedure)();

    this->detectionIndex = this->detectionTable.table[this->detectionIndex].alt;
    if (this->detectionIndex == -1)
      return this->B2();
  }
}

LexerState Lexer::I1()
{
  return LexerState::I1;
}

LexerState Lexer::I2()
{
  return LexerState::I2;
}

State S1a()
	{
		AddVariable();
		tokenValue = nameIndex;
		tokenClass = Variable_T;
		AddToken();
		return State::st_A1;
	}

// State S1b()
// 	{
// 		AddConstant();
// 		tokenValue = nameIndex;
// 		tokenClass = Constant_T;
// 		AddToken();
// 		return State::st_A1;
// 	}

// 	State S1c()
// 	{
// 		AddLongConstant();
// 		tokenValue = nameIndex;
// 		tokenClass = LongConstant_T;
// 		AddToken();
// 		return State::st_A1;
// 	}

LexerState Lexer::S1d()
{
  this->AddLabel();
  this->tokenValue = this->nameIndex;
  this->tokenClass = tkns::TokenClass::Label;
  this->AddToken();
  return LexerState::A1;
}

State S1e()
{
  switch (tokenClass)
  {
  case Jump_T:
    AddToken();
    return State::st_L1;
  default:
    AddToken();
    return State::st_A1;
  }
}

State S1h()
{
  if (tokenClass == Assignment_T)
    tokenValue = 0;

  AddToken();
  return State::st_A1;
}

LexerState Lexer::S1l()
{
  this->tokenClass = tkns::TokenClass::Comment;
  this->tokenValue = 0;
  this->AddToken();
  return LexerState::A1;
}

LexerState Lexer::Err1()
{
  this->tokenClass = tkns::TokenClass::Error;
  this->tokenValue = 0;
  this->AddToken();
  return LexerState::Err;
}

State Err2()
	{
		tokenClass = Error_T;
		tokenValue = 0;
		AddToken();
		lineNumber++;
		return State::st_A1;
	}
