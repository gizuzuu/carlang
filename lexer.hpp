#include "parsedprogram.hpp"
#include "polynomial.hpp"
#include "tokens.hpp"
#include <vector>

namespace lex
{
  const int DETECTION_TABLE_SIZE = 43;

  enum LexerState
  {
    A1,
    B1,
    B2,
    B3,
    C1,
    D1,
    D2,
    H1,
    H2,
    H3,
    H4,
    E1,
    I1,
    I2,
    G1,
    G2,
    G3,
    G4,
    L1,
    L2,
    Err,
    Stop
  };

  const int STATES_COUNT = LexerState::Stop;

  class Lexer
  {
  private:
    tkns::SymbolicToken symbolicToken;
    tkns::TokenClass tokenClass;

    int tokenValue;
    int lineNumber;

    int nameIndex;
    int number;
    tkns::CompareValue compareValue;

    std::string variableName;
    std::string label;

    pol::Polynomial polynomial;
    std::vector<tkns::Token> tokens;
    std::vector<tkns::ObjectName> nameTable;

    using parserProcedure = LexerState (Lexer::*)();
    parserProcedure table[STATES_COUNT][tkns::SYMBOL_TOKEN_CLASS_COUNT];

    struct DetectionTable
    {
      int initVector[26];

      struct DetectionTableLine
      {
        char letter;
        char alt;
        parserProcedure procedure;
      };
      DetectionTableLine table[DETECTION_TABLE_SIZE];
    };

    DetectionTable detectionTable;
    int detectionIndex;

  public:
    Lexer();

    void AddToken();
    void AddConstant();
    void AddPolynomialConstant();
    void AddVariable();
    void AddLabel();

    LexerState A1();
    LexerState A1a();
    LexerState A1b();
    LexerState A1c();
    LexerState A1d();
    LexerState A1e();
    LexerState A1f();
    LexerState A1g();
    LexerState A1h();
    LexerState A1l();
    LexerState B1();
    LexerState B1a();
    LexerState B1b();
    LexerState B2();
    LexerState B2a();
    LexerState B3();
    LexerState C1();
    LexerState C1a();
    LexerState C1b();
    LexerState D1();
    LexerState D1a();
    LexerState D2();
    LexerState D2a();
    LexerState G1();
    LexerState G1a();
    LexerState G2();
    LexerState G2a();
    LexerState G3();
    LexerState G3a();
    LexerState G3b();
    LexerState G4();
    LexerState G4a();
    LexerState H1();
    LexerState H1a();
    LexerState H1b();
    LexerState H2();
    LexerState H2a();
    LexerState H3();
    LexerState H3a();
    LexerState H4();
    LexerState H4a();
    LexerState S1d();
    LexerState S1l();
    LexerState E1();
    LexerState E1a();
    LexerState I1();
    LexerState I1a();
    LexerState I2();
    LexerState I2a();
    LexerState L1();
    LexerState L1a();
    LexerState L2();
    LexerState L2a();
    LexerState M1();
    LexerState Err1();

    tkns::SymbolicToken Transliterate(int symbol);
    ParsedLexerProgram Run(std::string filename);
  };
}
