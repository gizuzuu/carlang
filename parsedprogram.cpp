#include "parsedprogram.hpp"

using namespace tkns;

ParsedLexerProgram::ParsedLexerProgram(std::string filename)
{
    this->programName = filename;
    this->tokens.push_back(Token(TokenClass::Error, 0, 0));
}

ParsedLexerProgram::ParsedLexerProgram(std::vector<Token> &&parsedTokens, std::vector<ObjectName> parsedNameTable, std::string filename)
{
    this->programName = filename;
    this->tokens = parsedTokens;
    this->nameTable = parsedNameTable;
}

ParsedLexerProgram::ParsedLexerProgram(ParsedLexerProgram&& program)
{
    this->programName = program.programName;
    program.programName = nullptr;

    tokens = std::move(program.tokens);
    nameTable = std::move(program.nameTable);
}

ParsedLexerProgram::~ParsedLexerProgram()
{
    // TODO
    for (auto& name : this->nameTable)
	{
		switch (name.GetType())
		{
		case ObjectType::Variable: 
            delete (std::string*)name.GetPointer(); 
            break;
		case ObjectType::Constant: 
            delete (int*)name.GetPointer(); 
            break;
		case ObjectType::Polynomial: 
            delete (pol::Polynomial*)name.GetPointer(); 
            break;
		case ObjectType::Label: 
            delete (std::string*)name.GetPointer(); 
            break;
		}
	}
}

std::ostream& ParsedLexerProgram::PrintTokens(std::ostream& stream)
{
    int previousLine = tokens[0].GetLine(), currentLine;

    stream << "Tokens for '" << this->programName << "':" << std::endl;
    stream << tokens[0].GetLine() << ": ";

    for (auto token : this->tokens)
    {
        currentLine = token.GetLine();

        if (currentLine != previousLine)
            stream << std::endl << currentLine << ": ";

        stream << token << " ";
        previousLine = currentLine;
    }
    
    return stream;
}

std::ostream& ParsedLexerProgram::PrintNames(std::ostream& stream)
{
    stream << "Names for '" << this->programName << "':" << std::endl;

    for (long unsigned int i = 1; i <= this->nameTable.size(); ++i)
    {
        stream << i << ". " << this->nameTable[i-1] << std::endl;
    }
    
    return stream;
}

std::ostream& ParsedLexerProgram::PrintErrors(std::ostream& stream)
{
    int errorsCount = 0;
    stream << "Errors for '" << this->programName << "':" << std::endl;

    for (auto token : this->tokens)
    {
        if (token.GetClass() != TokenClass::Error) continue;

        errorsCount++;
        stream << "Error at line " << token.GetLine() << ": " << token.GetValue() << std::endl;
    }

    if (errorsCount)
        stream << "Total errors count: " << errorsCount;
    else
        stream << "No errors, very good!";
    
    return stream;
}
