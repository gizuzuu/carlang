#pragma once

#include <vector>
#include "tokens.hpp"

class ParsedLexerProgram
{
private:
    std::string programName;
    std::vector<tkns::Token> tokens;
    std::vector<tkns::ObjectName> nameTable;

public:
    ParsedLexerProgram(std::string filename);
    ParsedLexerProgram(std::vector<tkns::Token> &&parsedTokens, std::vector<tkns::ObjectName> parsedNameTable, std::string filename);
    ParsedLexerProgram(ParsedLexerProgram&& program);

    std::ostream& PrintTokens(std::ostream& stream);
    std::ostream& PrintNames(std::ostream& stream);
    std::ostream& PrintErrors(std::ostream& stream);

    ~ParsedLexerProgram();
};
