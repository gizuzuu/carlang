#include "polynomial.hpp"
#include <cmath>
#include <filesystem>
#include <istream>
#include <ostream>

using namespace pol;

Polynomial::Polynomial(int number)
{
  this->head = new Node;
  this->head->term.Set(0, number);
  this->head->next = nullptr;
}

Polynomial::Polynomial(int power, float coefficient)
{
  this->head = new Node;
  this->head->term.Set(power, coefficient);
  this->head->next = nullptr;
}

Polynomial::Polynomial(const Polynomial& other)
{
  if (other.head == nullptr)
  {
    this->head = nullptr;
    return;
  }

  this->head = new Node;
  head->term = other.head->term;
  head->next = nullptr;

  Node* temp = this->head;
  for (Iterator iterator = ++other.begin(); iterator != other.end(); ++iterator)
  {
    temp->next = new Node;
    temp = temp->next;
    temp->term = *iterator;
    temp->next = nullptr;
  }
}

Polynomial::Polynomial(Polynomial&& other)
{
  this->head = other.head;
  other.head = nullptr;
}

void Polynomial::AddTerm(int power, float coefficient)
{
  if (coefficient == 0) return;

  Node* new_node = new Node;
  new_node->term.Set(power, coefficient);

  if (this->head == nullptr)
  {
    this->head = new_node;
    this->head->next = nullptr;
    return;
  }

  Node* current = this->head;
  Node* previous = nullptr;

  while (current && current->term.power < power) 
  {
    previous = current;
    current = current->next;
  }

  if (current)
  {
    if (current->term.power == power)
    {
      current->term.coefficient = coefficient;
    }
    else if (current == this->head)
    {
      new_node->next = this->head;
      this->head = new_node;
    }
    else 
    {
      previous->next = new_node;
      new_node->next = current;
    }
  }
  else 
  {
    previous->next = new_node;
    new_node->next = nullptr;
  }
}

Term& Polynomial::GetTerm(int power)
{
  for (auto& term : *this)
    if (term.power == power) 
      return term;

  throw power;
}

int Polynomial::GetDegree() const
{
  if (this->head == nullptr) return 0;

  Node* temp = this->head;
  while(temp->next != nullptr)
    temp = temp->next;

  return temp->term.power;
}

Polynomial Polynomial::GetDerivative()
{
  Polynomial derrivative;

  for (auto& term : *this)
  {
    if (term.power == 0) continue;

    derrivative.AddTerm(term.power-1, term.power * term.coefficient);
  }

  return derrivative;
}

void Polynomial::Clear()
{
  while(this->head)
  {
    Node* node = head;
    head = head->next;
    delete node;
  }
}

Polynomial& Polynomial::operator=(const Polynomial& other)
{
  if (this == &other) return *this;

  this->Clear();

  if (other.head == nullptr)
  {
    this->head = nullptr;
    return *this;
  }

  this->head = new Node;
  this->head->term = other.head->term;
  this->head->next = nullptr;

  Node* temp = this->head;
  for (Iterator iterator = ++other.begin(); iterator != other.end(); ++iterator)
  {
    temp->next = new Node;
    temp = temp->next;
    temp->term = *iterator;
    temp->next = nullptr;
  }

  return *this;
}

Polynomial& Polynomial::operator=(Polynomial&& other)
{
  if (this == &other) return *this;

  this->Clear();

  this->head = other.head;
  other.head = nullptr;

  return *this;
}

Polynomial Polynomial::operator+(const Polynomial& right) const
{
  Polynomial sum;

  auto max = [](int a, int b) { return a > b ? a : b; };

  for (int power = 0; power <= max(this->GetDegree(), right.GetDegree()); ++power)
  {
    float coefficient = this->operator[](power) + right[power];
    sum.AddTerm(power, coefficient);
  }

  return sum;
}

Polynomial Polynomial::operator-(const Polynomial& right) const
{
  Polynomial dif;

  auto max = [](int a, int b) { return a > b ? a : b; };

  for (int power = 0; power <= max(this->GetDegree(), right.GetDegree()); ++power)
  {
    float coefficient = this->operator[](power) - right[power];

    dif.AddTerm(power, coefficient);
  }

  return dif;
}

Polynomial Polynomial::operator*(const Polynomial& right) const
{
  Polynomial prod;

  for (int power = 0; power <= this->GetDegree() + right.GetDegree(); ++power)
  {
    float coefficient = 0;

    int power1, power2;
    for (power1 = 0, power2 = power; power1 <= power; power1++, power2++)
    {
      coefficient += this->operator[](power1) + right[power2];
    }

    prod.AddTerm(power, coefficient);
  }

  return prod;
}

Polynomial Polynomial::operator/(const Polynomial& right) const
{
  Polynomial quot;
  Polynomial remainder(*this);
  int power; float coefficient;

  while (remainder.GetDerivative() >= right.GetDegree())
  {
    power = remainder.GetDegree() - right.GetDegree();
    coefficient = remainder[remainder.GetDegree()] / right[right.GetDegree()];

    quot.AddTerm(power, coefficient);

    remainder = remainder - Polynomial(power, coefficient) * right;
  }

  return quot;
}

Polynomial Polynomial::operator%(const Polynomial& right) const
{
  Polynomial remainder(*this);
  int power; float coefficient;

  while (remainder.GetDegree() >= right.GetDegree())
  {
    power = remainder.GetDegree() - right.GetDegree();
    coefficient = remainder[remainder.GetDegree()] / right[right.GetDegree()];

    remainder = remainder - Polynomial(power, coefficient) * right;
  }

  return remainder;
}

bool Polynomial::operator==(const Polynomial& other) const
{
  Node* temp1 = this->head;
  Node* temp2 = other.head;

  while (temp1 && temp2)
  {
    if (temp1->term != temp2->term) return false;

    temp1 = temp1->next;
    temp2 = temp2->next;
  }

  return !(temp1 || temp2);
}

bool Polynomial::operator!=(const Polynomial& other) const
{
  return !this->operator==(other);
}

float Polynomial::operator()(int x)
{
  float val;
  float term_val;

  for (auto& term : *this)
  {
    term_val = term.coefficient;

    for (int i = 0; i < term.power; ++i)
      term_val *= x;

    val += term_val;
  }

  return val;
}

float Polynomial::operator[](int power) const
{
  for (auto& term : *this)
    if (term.power == power)
      return term.coefficient;

  return 0;
}

std::ostream& operator<<(std::ostream& stream, const Polynomial& polynomial)
{
  stream << "[[";

  for (auto& term : polynomial)
  {
    if (term.coefficient >= 0) stream << "+" << term.coefficient;
    else stream << term.coefficient;

    stream << "x" << term.power;
  }

  stream << "]]";

  return stream;
}

std::istream& operator>>(std::istream& stream, Polynomial& polynomial)
{
  polynomial.Clear();

  try {
    while(stream.peek() == ' ' || stream.peek() == '\n' || stream.peek() == '\t')
      stream.ignore();

    if (stream.get() != '[') throw 1;
    if (stream.get() != '[') throw 1;

    int power; float coefficient;

    while(stream.peek() != ']')
    {
      stream >> coefficient;
      if (stream.fail()) throw 1;

      if (stream.get() != 'x') throw 1;

      stream >> power;
      if (stream.fail()) throw 1;

      polynomial.AddTerm(power, coefficient);
    }

    if (stream.get() != ']') throw 1;
    if (stream.get() != ']') throw 1;
  } catch (...) {
    stream.setstate(std::ios::failbit);
  }

  return stream;
}
