#pragma once
#include <exception>
#include <iostream>
#include <ostream>

namespace pol
{
  struct Term 
  {
    int power;
    float coefficient;

    void Set(int _power, float _coefficient)
    {
      power = _power;
      coefficient = _coefficient;
    }

    bool operator==(const Term& other)
    {
      if (power != other.power) return false;
      if (coefficient != other.coefficient) return false;

      return true;
    }

    bool operator!=(const Term& other)
    {
      return !operator==(other);
    }
  };

  class Polynomial
  {
  private:
    struct Node 
    {
      Term term;
      Node* next;
    };

    Node* head;

  public:
    Polynomial()
    {
      this->head = nullptr;
    }
    Polynomial(const Polynomial& other);
    Polynomial(Polynomial&& other);
    Polynomial(int number);
    Polynomial(int power, float coefficient);
    ~Polynomial()
    {
      this->Clear();
    }

    class Iterator 
    {
      private:
        Node* pointer;
      public:
        Iterator(Node* _pointer)
        {
          pointer = _pointer;
        }

        Term& operator*()
        {
          return pointer->term;
        }

        Iterator operator++()
        {
          pointer = pointer->next;
          return *this;
        }
        Iterator operator++(int)
        {
          Iterator old(pointer);
          pointer = pointer->next;
          return old;
        }

        bool operator==(const Iterator& other)
        {
          return pointer == other.pointer;
        }
        bool operator!=(const Iterator& other)
        {
          return pointer != other.pointer;
        }
    };

    Iterator begin() const { return this->head; }
    Iterator end() const { return nullptr; }

    void Clear();
    void AddTerm(int power, float coefficient);
    Term& GetTerm(int power);
    int GetDegree() const;
    Polynomial GetDerivative();

    Polynomial& operator=(const Polynomial& other);
    Polynomial& operator=(Polynomial&& other);
    float operator[](int power) const;
    float operator()(int x);
    Polynomial operator+(const Polynomial& right) const;
    Polynomial operator-(const Polynomial& right) const;
    Polynomial operator*(const Polynomial& right) const;
    Polynomial operator/(const Polynomial& right) const;
    Polynomial operator%(const Polynomial& right) const;
    
    bool operator==(const Polynomial& other) const;
    bool operator!=(const Polynomial& other) const;

    operator bool() { return head; }
  };
}

std::ostream& operator<<(std::ostream& stream, const pol::Polynomial& polynomial);
std::istream& operator>>(std::istream& stream, pol::Polynomial& polynomial);