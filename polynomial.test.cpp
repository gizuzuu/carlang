#include "polynomial.hpp"

using namespace pol;

int main (int argc, char *argv[]) {
  Polynomial pol1, pol2;

  pol1.AddTerm(2, 1);
  pol1.AddTerm(1, 4);
  pol2.AddTerm(1, 10);

  std::cout << "p1: " << pol1 << std::endl;
  std::cout << "p2: " << pol2 << std::endl;

  std::cout << "p1 + p2 = " << (pol1+pol2) << std::endl;
  std::cout << "p1 - p2 = " << (pol1-pol2) << std::endl;
  std::cout << "p1 * p2 = " << (pol1*pol2) << std::endl;

  return 0;
}
