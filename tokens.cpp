#include <string>
#include "tokens.hpp"

using namespace tkns;

// Token out
std::ostream& operator<<(std::ostream& stream, Token& token)
{
    switch (token.GetClass())
    {
    case TokenClass::Int:
        stream << "int";
        break;
    case TokenClass::Polynomial:
        stream << "polynomial";
        break;
    case TokenClass::Declare:
        stream << "declare";
        break;
    case TokenClass::Set:
        stream << "set";
        break;
    case TokenClass::Assigment:
        stream << "=";
        break;
    case TokenClass::Begin:
        stream << "begin";
        break;
    case TokenClass::Cin:
        stream << "cin";
        break;
    case TokenClass::Comment:
        stream << "comment";
        break;
    case TokenClass::Cout:
        stream << "cout";
        break;
    case TokenClass::Else:
        stream << "else";
        break;
    case TokenClass::End:
        stream << "end";
        break;
    case TokenClass::Eof:
        stream << "end of file";
        break;
    case TokenClass::Error:
        stream << "error";
        break;
    case TokenClass::If:
        stream << "if";
        break;
    case TokenClass::Jump:
        stream << "jump";
        break;
    case TokenClass::Label:
        stream << "label";
        break;
    case TokenClass::StreamIn:
        stream << ">>";
        break;
    case TokenClass::StreamOut:
        stream << "<<";
        break;
    case TokenClass::Then:
        stream << "then";
        break;
    case TokenClass::CompareOperator:
        switch (token.GetValue())
        {
        case CompareValue::Bigger:
            stream << ">";
            break;
        case CompareValue::BiggerOrEqual:
            stream << ">=";
            break;
        case CompareValue::Equal:
            stream << "==";
            break;
        case CompareValue::Less:
            stream << "<";
            break;
        case CompareValue::LessOrEqual:
            stream << "<=";
            break;
        case CompareValue::NotEqual:
            stream << "!=";
            break;
        default:
            stream << "Unknown compare operator";
            break;
        }
        break;
    default:
        stream << "Unknown token";
        break;
    }

    return stream;
}

std::ostream& operator<<(std::ostream& stream, ObjectName& obj)
{
    switch (obj.GetType())
    {
    case ObjectType::Variable:
        stream << "Variable: " << *((std::string*)obj.GetPointer());
        break;
    case ObjectType::Constant:
        stream << "Variable: " << *((int*)obj.GetPointer());
        break;
    case ObjectType::Polynomial:
        stream << "Polynomial: " << *((pol::Polynomial*)obj.GetPointer()); 
        break;
    case ObjectType::Label:
        stream << "Label: " << *((std::string*)obj.GetPointer()); 
        break;
    default:
        break;
    }

    return stream;
}

TokenClass Token::GetClass()
{
    return this->clazz;
}

int Token::GetLine()
{
    return this->line;
}

int Token::GetValue()
{
    return this->value;
}

ObjectType ObjectName::GetType()
{
    return this->type;
}

void* ObjectName::GetPointer()
{
    return this->namePointer;
}
