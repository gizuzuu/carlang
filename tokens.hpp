#pragma once
#include <iostream>
#include "polynomial.hpp"

namespace tkns {

enum SymbolicTokenClass
{
    Letter_ST, Digit_ST, Arithmetic_ST, Compare_ST, Space_ST, LF_ST, Semicolon_ST,
    Eof_ST, Comma_ST, Colon_ST, LSqBracket_ST, RSqBracket_ST, LBracket_ST, RBracket_ST, Dot_ST, Other_ST
};

const int SYMBOL_TOKEN_CLASS_COUNT = SymbolicTokenClass::Other_ST + 1;

struct SymbolicToken
{
    SymbolicTokenClass clazz;
    int value;
};

enum CompareValue
{
    // !
    Negation, 
    // ==
    Equal, 
    // !=
    NotEqual, 
    // < 
    Less,
	// <=
    LessOrEqual, 
    // >
    Bigger, 
    // >=
    BiggerOrEqual
};

enum TokenClass
{
    Int, Polynomial, Declare, Set, If, Then, Else,
    Cin, Cout, StreamIn, StreamOut, Label, Jump,
    Comment, Begin, End,

    Variable, Assigment, CompareOperator, Constant, ArithmeticOperator,

    Error, Eof
};

enum class ObjectType
{
    Variable, Constant, Label, Polynomial
};

class Token
{
private:
    TokenClass clazz;
    int value;
    int line;
public:
    Token(TokenClass clazz, int value, int line)
    {
        this->clazz = clazz;
        this->value = value;
        this->line = line;
    }

    TokenClass GetClass();
    int GetLine();
    int GetValue();
};

class ObjectName
{
private:
    ObjectType type;
    void* namePointer;
public:
    ObjectName(ObjectType type, void* namePointer)
    {
        this->type = type;
        this->namePointer = namePointer;
    }

    ObjectType GetType();
    void* GetPointer();
};
};

std::ostream& operator<<(std::ostream& stream, tkns::ObjectName& obj);
std::ostream& operator<<(std::ostream& stream, tkns::Token& token);
